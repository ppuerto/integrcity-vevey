```bash
$ git clone https://gitlab.com/ppuerto/integrcity-vevey.git
$ cd integrcity-vevey
$ virtualenv -p python3 ict
$ source ict/bin/activate
(ict) $ pip install -r requirements.txt
(ict) $ python -m ipykernel install --user --name=ict
```