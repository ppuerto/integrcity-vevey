import os
import click
import pandas as pd, numpy as np

data_folder = os.path.abspath(os.path.dirname(__file__))

data_ss = pd.read_csv(os.path.join(data_folder, "design_ss.csv"))
data_hx = pd.read_csv(os.path.join(data_folder, "design_hx.csv"))
data_hp = pd.read_csv(os.path.join(data_folder, "design_hp.csv"))

# [(nbr, %P_tot)]
r = 1.0
DESIGN_RULES = [(1, r)] + [(n, r*round(20 * 1.2 / n) / 20) for n in range(2, 10)]

# [(P_min, P_max)]
DESIGN_RNGES = [
    (u[0] * min(data_hp.P_nom_kW), u[0] * max(data_hp.P_nom_kW))
    for u in DESIGN_RULES
]

dt_hp_src = 4.0
dt_hp_snk = 6.0

def get_idx_design(p_nom):
    i = 0
    while not DESIGN_RNGES[i][0] <= p_nom <= DESIGN_RNGES[i][1]:
        i += 1
        assert i < len(
            DESIGN_RNGES
        ), "Nominal power outside of the design range !"
    return i

def design_hp(p_nom):
    rule = DESIGN_RULES[get_idx_design(p_nom)]
    p_dim = p_nom * rule[1]
    hp = data_hp.sort_values(by="P_nom_kW").iloc[
        (p_dim - data_hp["P_nom_kW"]).abs().argsort()[0]
    ]
    return rule[0], hp.Type

def design_ss(p_nom, a):
    # Hoval design rule a = 15L/kW
    v_nom = a * p_nom  
    nbr = 1
    if v_nom > max(data_ss.Volume_L):
        v_nom /= 2
        nbr = 2
    s = data_ss.sort_values(by="Volume_L").iloc[
        (v_nom - data_ss["Volume_L"]).abs().argsort()[0]
    ]
    return nbr, s.Type

def design_hex(hp_type):
    hp = data_hp.loc[data_hp.Type == hp_type].iloc[0]
    return hp.Type_Hex

def design_flows(hp_type, dt_net=5.0):
    hp = data_hp.loc[data_hp.Type == hp_type].iloc[0]
    return round(hp.m_dot_srce_4C * (dt_hp_src/dt_net), 3), hp.m_dot_srce_4C, hp.m_dot_sink_6C

def design_dhpu(p_nom, a):
    nbr_hp, type_hp = design_hp(p_nom)
    nbr_ss, type_ss = design_ss(p_nom, a)
    nbr_hx, type_hx = nbr_hp, design_hex(type_hp)
    return nbr_hx, type_hx, nbr_hp, type_hp, nbr_ss, type_ss

def models_dhpu(p_nom, dt_net=5, a=15):
    nbr_hx, type_hx, nbr_hp, type_hp, nbr_ss, type_ss = design_dhpu(p_nom, a)
    m_dot_pri, m_dot_sec, m_dot_ter = design_flows(type_hp, dt_net)
    UA_hex = data_hx.loc[data_hx.Type == type_hx, "UA"].values[0]*1e3
    H_ss , D_ss = data_ss.loc[data_ss.Type == type_ss, ["h_mm", "d_mm"]].values[0]*1e-3
    m_dot_pri, m_dot_sec, m_dot_ter = np.array([m_dot_pri, m_dot_sec, m_dot_ter]).round(3)
    return nbr_hp, type_hp, nbr_ss, type_ss, H_ss, D_ss, UA_hex, m_dot_pri, m_dot_sec, m_dot_ter

@click.command()
@click.option('--p_nom', default=10, help='[kW] Nominal heat power')
@click.option('--dt_net', default=5, help='[°C] Temperature difference for the district heating network')
@click.option('--ratio', default=10, help='[L/kW] Nominal heat power')
def main(p_nom, dt_net, ratio):
    print(models_dhpu(p_nom, dt_net, ratio))

if __name__ == '__main__':
    
    main()

    assert get_idx_design(150) == 1
    assert design_hp(150) == (2, 'ThermaliaDualH90')
    assert design_ss(150) == (1, 'EnerValG2500')
    assert design_hex("ThermaliaDualH90") == "M6-MFG 63PL"
    assert design_flows("ThermaliaDualH90") == (3.23, 4.037, 3.589)
    assert design_dhpu(150) == (2, 'M6-MFG 63PL', 2, 'ThermaliaDualH90', 1, 'EnerValG2500')
    assert models_dhpu(150) == (2, 'ThermaliaDualH90', 1, 2.512, 1.2, 22850.0, 3.23, 4.037, 3.589)
