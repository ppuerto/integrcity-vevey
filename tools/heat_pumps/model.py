import os
import pandas as pd, numpy as np

from scipy.interpolate import LinearNDInterpolator
from scipy.optimize import curve_fit

DATA_FILE = "operation.csv"

here = os.path.abspath(os.path.dirname(__file__))

data = pd.read_csv(os.path.join(here, DATA_FILE), index_col=0)


def interp_value(model, value, t_srce, t_sink, extrapolate=True):
    """
    Examples
    --------
    >>> from full_hoval_hp_data import interp_value
    >>> interp_value("ThermaliaTwin42", "COP", -5, 30.0)
    >>> interp_value("ThermaliaTwin42", ["Q_srce", "Q_sink", "P_elec"], -5, 30.0)
    """
    hp_data = data.loc[data.Model == model]
    points = hp_data[["T_srce", "T_sink"]].values
    values = hp_data[value].values
    f = LinearNDInterpolator(points, values)
    res = f((t_srce, t_sink)).round(2)
    if type(value) is list:
        if any([r != r for r in res]) and extrapolate:
            res = np.array(
                [extrap_value(model, v, t_srce, t_sink) for v in value]
            )
    else:
        if res != res and extrapolate:
            res = extrap_value(model, value, t_srce, t_sink)
    return res


def func(T, a, b, c, d):
    t_src, t_snk = T
    return a * t_src * t_snk + b * t_src + c * t_snk + d


def extrap_value(model, value, t_srce, t_sink):
    """
    Examples
    --------
    >>> from full_hoval_hp_data import extrap_value
    >>> extrap_value("ThermaliaTwin42", "COP", -5, 30.0)
    >>> extrap_value("ThermaliaTwin42", ["Q_srce", "Q_sink", "P_elec"], -5, 30.0)
    """
    hp_data = data.loc[data.Model == model]
    popt, pcov = curve_fit(
        func,
        (hp_data.T_srce.values, hp_data.T_sink.values),
        hp_data[value].values,
    )
    a, b, c, d = popt
    return round(func((t_srce, t_sink), a, b, c, d), 2)
