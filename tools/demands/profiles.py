import os
import numpy as np
import pandas as pd

FREQ = "15min"

data_folder = os.path.abspath(os.path.dirname(__file__))

filename = "weekly_base_profiles_SIA_2024_380-1.csv"
weekly = pd.read_csv(os.path.join(data_folder, filename), index_col=0)
weekly.index = pd.to_datetime(weekly.index)
weekly.columns = [int(c) for c in weekly.columns]
weekly.loc[weekly.index[-1] + pd.Timedelta("1h")] = weekly.iloc[0]
weekly = weekly.resample(FREQ).interpolate().iloc[:-1]
weekly["DoW"] = weekly.index.dayofweek - 2
weekly["time"] = weekly.index.time
weekly.reset_index(drop=True, inplace=True)

filename = "Timeseries_46.619_7.062_CM__1kWp_crystSi_14_37deg_-7deg_2007_2016.csv"
meteo = pd.read_csv(
    os.path.join(data_folder, filename),
    skiprows=10,
    skipfooter=10,
    engine="python",
    index_col=0,
)
meteo.index = pd.to_datetime(meteo.index, format="%Y%m%d:%H%M") + pd.DateOffset(
    minutes=5
)

meteo = meteo.resample(FREQ).interpolate()

t_ext = meteo.Tamb


def generate_synthetic_el_load(start, horizon, aff, sre):
    noisex = 10
    noisey = 0.25

    end = start + pd.Timedelta(horizon)
    dow = start.dayofweek
    t0 = start.time()
    n_days = (end - start) / np.timedelta64(1, "D")
    n_weeks = int(np.trunc(n_days / 7) + 1)

    idx_match = weekly.loc[(weekly.DoW == dow) & (weekly.time == t0)].index[0]
    base = weekly.loc[
        list(weekly.index[idx_match:]) + list(weekly.index[:idx_match]), int(aff)
    ].values
    shift = np.random.randint(-1 * noisex, noisex, size=n_weeks)
    weeks = [
        np.concatenate((base[shift[i] :], base[: shift[i]]))
        * np.random.uniform(low=1 - noisey, high=1 + noisey, size=len(weekly))
        for i in range(n_weeks)
    ]
    profile = np.concatenate(weeks) * sre * 1e-3  # to kW
    return pd.Series(
        profile, index=pd.date_range(start=start, freq=FREQ, periods=len(profile))
    ).loc[start:end]


def generate_synthetic_th_load(start, horizon, aff, sre, p_nom=None):
    if not p_nom:
        # TODO: use aff and sre to determine p_nom
        p_nom = 10  # [kW]

    t_dim = -12
    t_max = 12
    lag = "{}h".format(np.random.randint(2, 12))
    min_load = 0.25
    noise = 0.1

    rd_t_ext = t_ext * np.random.uniform(low=1 - noise, high=1 + noise, size=len(t_ext))
    mm_t_ext = rd_t_ext.rolling(int(pd.Timedelta(lag) / pd.Timedelta(FREQ))).mean()
    mm_t_ext.fillna(inplace=True, method="bfill")
    mm_t_ext.fillna(inplace=True, method="ffill")
    a, b = np.polyfit([t_dim, t_max], [1, min_load], 1)
    load = np.clip(a * mm_t_ext + b, 0, 1)
    load.loc[load < min_load] = 0.0
    end = start + pd.Timedelta(horizon)
    return 0.75 * p_nom * load.loc[start:end]


def generate_synthetic_hw_load(start, horizon, aff, sre, p_nom=None, dhw_needs=None):
    if not p_nom:
        # TODO: use aff and sre to determine p_nom
        p_nom = 10  # [kW]

    if not dhw_needs:
        # TODO: use aff and sre to determine p_nom
        dhw_needs = p_nom * 500  # [kWh]

    end = start + pd.Timedelta(horizon)

    loss = 0.2
    prob_day_15min = (
        (1 + loss)
        * dhw_needs
        / p_nom
        * (pd.Timedelta("1h") / pd.Timedelta("15min"))
        / 365
    )

    daytime = int(pd.Timedelta("1D") / pd.Timedelta("15min"))
    s = np.random.poisson(prob_day_15min, pd.Timedelta(horizon).days)
    t = np.random.randint(daytime, size=sum(s))

    index = pd.date_range(start=start, end=end, freq="15min")
    dhw = pd.Series([0] * len(index), index=index)

    for day in range(pd.Timedelta(horizon).days):
        for draw in range(s[day]):
            idx = sum(s[:day]) + draw
            dhw.iloc[day * daytime + t[idx]] = np.random.uniform(0.9, 1.1)

    return 0.90 * dhw * p_nom
