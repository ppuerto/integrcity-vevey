import networkx as nx


def hp(sink, cop=3.5):
    elec = sink / cop
    srce = sink - elec
    return srce, elec


def chp(hsnk, heff=0.40, eeff=0.45):
    srce = hsnk / heff
    esnk = srce * eeff
    return srce, esnk


def dhn(sink, eff=0.9):
    srce = sink / eff
    return srce


def chain(x, operations):
    for operation in operations:
        x = operation(x)
    return x


def run_flows(g, init_values):
    sinks = [n for n in g.nodes if len(list(g.successors(n))) == 0]
    srces = init_values.keys()
    assert set(srces) == {n for n, d in g.in_degree() if d == 0}
    res = {}
    for snk in sinks:
        dict_fun = {}
        for src in srces:
            try:
                path = nx.shortest_path(g, src, snk)
                dict_fun[path[0]] = [
                    g[path[i - 1]][path[i]]["fun"] for i in range(1, len(path))
                ]
            except nx.NetworkXNoPath:
                pass
        res[snk] = sum(
            [chain(init_values[key], operations) for key, operations in dict_fun.items()]
        )
    return res


if __name__ == "__main__":
    h = nx.DiGraph()

    values = {"A": np.array([5.0, 1.0]), "B": np.array([2.0, 1.0])}

    h.add_edge("A", "C", fun=lambda x: x * 2)
    h.add_edge("B", "C", fun=lambda x: x + 2)
    h.add_edge("C", "D", fun=lambda x: x - 1)
    h.add_edge("C", "E", fun=lambda x: x / 5)

    print(run_flows(h, values))
