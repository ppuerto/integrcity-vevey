CRS = {'datum': 'WGS84', 'ellps': 'WGS84', 'proj': 'utm', 'zone': 32, 'units': 'm'}

T_lac_sup = 10.0  # °C
T_net_sup = 25.0  # °C
T_net_ret = 18.0  # °C

dT_pinch = 5.0    # °C

T_hps_sup = T_net_sup - dT_pinch
T_hps_ret = 16.0  # °C
T_htn_sup = 80.0  # °C

dT_net = T_net_sup - T_net_ret